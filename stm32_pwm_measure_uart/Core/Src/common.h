/*
 * common.h
 *
 *  Created on: 24 Nov 2020
 *      Author: jakub
 */

#ifndef SRC_COMMON_H_
#define SRC_COMMON_H_

#define LED_PORT GPIOC
#define LED_PIN GPIO_PIN_13

#define LED_ON() HAL_GPIO_WritePin(LED_PORT,LED_PIN, GPIO_PIN_SET)
#define LED_OFF() HAL_GPIO_WritePin(LED_PORT,LED_PIN, GPIO_PIN_RESET)
#define LED_TOGGLE() HAL_GPIO_TogglePin(LED_PORT, LED_PIN)



#define RES_270R_PORT		GPIOB
#define RES_270R_PIN		GPIO_PIN_12

#define RES_2K74_PORT		GPIOB
#define RES_2K74_PIN		GPIO_PIN_14

#define RES_1K3_PORT		GPIOB
#define RES_1K3_PIN			GPIO_PIN_13




#define Res_270R_set(state) (HAL_GPIO_WritePin(RES_270R_PORT, RES_270R_PIN, state))
#define Res_1K3_set(state) (HAL_GPIO_WritePin(RES_1K3_PORT, RES_1K3_PIN, state))
#define Res_2K74_set(state) (HAL_GPIO_WritePin(RES_2K74_PORT, RES_2K74_PIN, state))


#define ADC_SCALE_FACTOR 3.3/4096.0
#define VOLTAGE_DIVIDER_SCALE_FACTOR 4.28f

#define UART_INT_PRIO 0
#define ADC_INT_PRIO 3
#define PWM_EDGE_DETECT_PRIO 1
#define TIM3_DELAY_BEFORE_MEASURE_PRIO 2
#define TIM2_WATCHDOG_CONST_SIG_PRIO 2


#define RECV_BUFF_SIZE 128
#define REQUEST_FRAME_SIZE 8
#define PWM_AMP_PROBE_LEN 8
#define PWM_MEASUREMENTS_PROBES 4



#define PP_TEMP_MEASUREMENTS_BUF_LEN 3
#define ADC_SCALE_FACTOR 3.3/4096.0
#define VOLTAGE_DIVIDER_SCALE_FACTOR 4.28f
#define PP_VOLTAGE_SCALE_FACTOR 2
#define PP_CURRENT 1.57f


#define DLE_VAL 0x10
#define STX_VAL 0x02
#define ETX_VAL 0x03

#define LOW_LEVEL_TRESHOLD 50

#endif /* SRC_COMMON_H_ */
