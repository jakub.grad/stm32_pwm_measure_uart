/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "common.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */



volatile uint8_t recv_buf[RECV_BUFF_SIZE];

volatile uint8_t uart_write_ptr = 0;
volatile uint8_t uart_read_ptr = 0;


volatile uint16_t pwm_amp_probe[PWM_AMP_PROBE_LEN];
volatile uint16_t pwm_measurements_probes[PWM_MEASUREMENTS_PROBES * PWM_AMP_PROBE_LEN];
volatile uint8_t  pwm_new_probe_ptr = 0;

volatile uint16_t InputPWMPeriod_cycles; // Czas trwania calego okresu sygnalu [w cyklach zegara]
volatile double InputPWMPeriod_miliseconds; // Czas trwania calego okresu sygnalu [w milisekundach]
volatile uint16_t InputPWMDuty_cycles; // Czas trwania stanu wysokiego [w cyklach zegara]
volatile uint8_t InputPWMDuty_percent; // Wspolczynnik wypelnienia sygnalu PWM [w procentach]
volatile double InputPWMFrequency = 0;
volatile uint16_t InputTimerResolution_cycles = 0; // Rozdzielczosc zegara odczytujacego sygnal PWM [w cyklach zegara] - TIM2 mierzy z czestotliwoscia 500 Hz
volatile double InputTimerPeriodDuration_miliseconds =0; // Czas trwania jednego okresu timera odczytujacego sygnal PWM [w milisekundach]




void tim3_adc_trigger_stop(void);

void tim3_adc_trigger_start(void);

typedef struct {
	uint16_t pwm_meas_period_prescaler_tim1;
	uint16_t pwm_meas_period_in_cycles_tim1;
	double pwm_meas_period_in_ms_tim1;

	uint16_t pwm_adc_take_probe_prescaler_tim3;
	uint16_t pwm_adc_take_probe_period_in_cycles_tim3;

	uint16_t const_signal_watchdog_period_in_cycles_tim2;
	uint16_t const_signal_watchdog_prescaler_tim2;

}peripheral_config_t;

peripheral_config_t p_config;


typedef struct {
	uint8_t pwm_measure_started : 1;
	uint8_t const_signal_measure_started: 1;
	uint8_t request_received : 1;
	uint8_t const_signal_measure_mode : 1;
	uint8_t frame_error : 1;
	uint8_t ignore_watchdog_tim2_interrupt : 1;
	uint8_t uart_buf_wrapped_around : 1;
	uint8_t : 1; //do wykorzystania

}device_status_t;

volatile device_status_t dev_status;



typedef struct {	//struktura zawierajaca wyniki pomiarowe

    uint32_t HeaderPacked;                      //naglowek zwiekszany + 1
    uint16_t cmdStruct;                         //struct type
    uint16_t sizeStruct;                        //data size

	uint16_t resPP;							//rezystancja PP w Ohm
	uint16_t pwmFrq;						//PWM częstotliwosc [Hz]
	uint16_t pwmDuty;						//PWM wypelnienie [%]
	uint16_t  pwmVoltage_HIGH;				//PWM poziom napiecia [mv]
	uint16_t  pwmVoltage_LOW;				//PWM poziom napiecia [mv]

	uint16_t tempSensor1;					//temperatura czujnik [mC]
	uint16_t tempSensor2;					//temperatura czujnik [mC]

	uint16_t resSensor1;
	uint16_t resSensor2;

	uint16_t CRC16;

	//uint16_t : 16; //padding

}measure_frame_t;


volatile measure_frame_t pwm_mframe;

#define DLE_VAL 0x10
#define STX_VAL 0x02
#define ETX_VAL 0x03


typedef struct {

	uint8_t DLE1;
	uint8_t STX;
	uint16_t order;
	uint16_t crc;
	uint8_t DLE2;
	uint8_t ETX;

}request_msg_t ;



volatile request_msg_t request;


typedef struct {
	double temp1_vol;
	double temp2_vol;
	double pp_vol;
	uint64_t : 64; //padding

}measure_pp_temp_t;


measure_pp_temp_t pp_temp_mframe;


typedef enum {
	EV_STATE_A = 0,
	NO_CHANGE = 1,
	EV_STATE_B = 2,
	EV_STATE_C = 3,
	EV_STATE_D = 4,
}EV_STATE_t;


enum adc2_channel_choose_t { NO_CAHNNEL,TEMP1_CHANNEL, TEMP2_CHANNEL, PP_CHANNEL};


void peripheral_config(void) {
	//TIM 1 zasilany z magistrali APB2 czyli 32 MHz


	p_config.pwm_meas_period_in_cycles_tim1 = 45714;
	p_config.pwm_meas_period_prescaler_tim1 = 1;
	p_config.pwm_meas_period_in_ms_tim1 = 1000.0 / ((HAL_RCC_GetSysClockFreq()/2)/
			( p_config.pwm_meas_period_prescaler_tim1 * 1.0) /
			( p_config.pwm_meas_period_in_cycles_tim1 * 1.0));


	p_config.pwm_adc_take_probe_prescaler_tim3 = 64;
	p_config.pwm_adc_take_probe_period_in_cycles_tim3 = 30;
	//p_config.pwm_adc_take_probe_prescaler_tim3 = 6400;
	//p_config.pwm_adc_take_probe_period_in_cycles_tim3 = 10000;

	//700 Hz - czestotliwosc watchdoga wykrywajacego zanik pwm. timer taktowany magistrala 64 MHz
	p_config.const_signal_watchdog_period_in_cycles_tim2 = 11428;
	p_config.const_signal_watchdog_prescaler_tim2 = 8;


	InputTimerResolution_cycles = p_config.pwm_meas_period_in_cycles_tim1;
	InputTimerPeriodDuration_miliseconds = p_config.pwm_meas_period_in_ms_tim1;


}



uint16_t calculateCRC16(uint8_t *data, uint16_t length)
{
	uint16_t crc_val = 0;

	for(uint16_t j = 0; j < length; j++)
	{
		crc_val ^= data[j];

		for(uint8_t i = 0; i < 8; i++)
		{
			if ((crc_val & 0x0001) > 0)
		 	{
				crc_val >>= 1;
		 	    crc_val ^= 0xA001;
		 	}
		 	else
		 	{
		 		crc_val >>= 1;
		 	}
		}
	}
	return crc_val;
}


bool validate_request(void) {

	if( (request.DLE1 == DLE_VAL) &&
			(request.DLE2 == DLE_VAL) &&
			(request.STX == STX_VAL) &&
			//(request.ETX == ETX_VAL)) {
			(request.ETX == ETX_VAL)
			&& (calculateCRC16((uint8_t *)&(request.order),2*sizeof(uint16_t)) == 0 )) {
				return true;

	}
   return false;
}


request_msg_t * get_request_msg(void) {
	return &request;
}




uint16_t calcAvgPulseHigh(uint16_t * arr,uint16_t len) {
	uint16_t j = 0;
	uint16_t avg = 0;
	for(uint16_t i =0; i<len; ++i) {
		if(arr[i] > LOW_LEVEL_TRESHOLD) {
			avg += arr[i];
			j++;
		}
	}
	if(j) avg /= j;
	return avg;
}



void uart_dma_enable(void) {
	if ((USART1->CR1 & USART_CR1_UE) == 0) {
		USART1->CR1 |= USART_CR1_UE;
		USART1->CR1 |= (USART_CR1_IDLEIE);
		while((USART1->CR1 & USART_CR1_UE) == 0 );
	}
}

void uart_dma_disable(void) {
	if ((USART1->CR1 & USART_CR1_UE) == USART_CR1_UE) {
		USART1->CR1 &= ~(USART_CR1_IDLEIE);
		USART1->CR1 &= ~(USART_CR1_UE);
		while((USART1->CR1 & USART_CR1_UE) == USART_CR1_UE);
	}
}





#define UART_DMA_INT_MODE 0

void uart_dma_init(void) {

	if ((RCC->APB2ENR & RCC_APB2ENR_IOPAEN) == 0) {
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	}



	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	memset((uint8_t*) &GPIO_InitStruct, 0, sizeof(GPIO_InitTypeDef));

	// __HAL_RCC_USART1_CLK_ENABLE();
	//__HAL_RCC_GPIOA_CLK_ENABLE();

	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_10;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	if ((RCC->AHBENR & RCC_AHBENR_DMA1EN) == 0) {
		//wlaczenie zegara DMA
		RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	}

	//wylaczenie DMA (przy reinicjalizacji potrzebne)
	DMA1_Channel5->CCR &= ~(DMA_CCR_EN);

	//Przypisanie adresu zrodlowego peryferium skad pobierane beda dane i transferowane do pamieci
	DMA1_Channel5->CPAR = (uint32_t) (&(USART1->DR));
	//przypisanie adresu pamieci do ktorego zapisywane beda dane
	DMA1_Channel5->CMAR = (uint32_t) (recv_buf);
	//ilosc transakcji DMA (tozsame z rozmiarem bufora). Gdy wszystkie transakcje zostana wywolane
	//to ten rejestr ma wartosc 0. Zadana nowa transkacja nie moze byc obsluzona jezeli ponownie sie
	//go nie zaprogramuje na niezerowa wartosc. Aby to zrobic wczesniej trzeba wylaczyc DMA, poniewaz
	//gdy DMA jest wlaczone ten rejestr jest tylko read-only

	DMA1_Channel5->CNDTR = RECV_BUFF_SIZE;

	//najwyzszy priorytet transakcji DMA
	DMA1_Channel5->CCR |= (DMA_CCR_PL_0 | DMA_CCR_PL_1);
	//auto inkrementacja adresu pamieci do ktorego ladowane beda dane
	//wlaczenie przerwan od ukonczenia transferu - przerwanie generowane jest po wszystkich transakcjach okreslonych przez CNDTR

#if UART_DMA_INT_MODE == 1
	DMA1_Channel5->CCR |= (DMA_CCR_MINC | DMA_CCR_TCIE | DMA_CCR_CIRC);
#else
	DMA1_Channel5->CCR |= (DMA_CCR_MINC | DMA_CCR_CIRC);
#endif

	//wlaczenie DMA
	DMA1_Channel5->CCR |= DMA_CCR_EN;

	//wlaczenie zegara UART1
	if ((RCC->APB2ENR & RCC_APB2ENR_USART1EN) == 0) {
		RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	}

	uart_dma_disable();

	RCC->APB2RSTR |= RCC_APB2RSTR_USART1RST;
	HAL_Delay(1);
	RCC->APB2RSTR &= ~(RCC_APB2RSTR_USART1RST);

	//8 bit ramka
	//USART1->CR1 = 0;
	//16 MHz / 115200
	USART1->BRR = (160000 / 1152);
	//1 bit stopu
	//USART1->CR2 = 0;
	//wlaczneie przerwan od odbioru oraz wlacznie odbiornika
	//USART1->CR1 |= USART_CR1_RXNEIE | USART_CR1_RE;
	//USART1->CR1 |= USART_CR1_RE | USART_CR1_TE;
	USART1->CR1 |= USART_CR1_RE | USART_CR1_TE | USART_CR1_IDLEIE;
	//flow control wylaczony

	// USART1->CR3 = 0;
	//wlaczenie receivera DMA
	USART1->CR3 |= USART_CR3_DMAR;

	//uruchomienie przerwan w kontrolerze NVIC
#if UART_DMA_INT_MODE == 1
	if (__NVIC_GetEnableIRQ(DMA1_Channel5_IRQn) == 0) {
		NVIC_EnableIRQ(DMA1_Channel5_IRQn);
		NVIC_SetPriority(DMA1_Channel5_IRQn, UART_INT_PRIO);
	}
#endif

	if (__NVIC_GetEnableIRQ(USART1_IRQn) == 0) {
		NVIC_EnableIRQ(USART1_IRQn);
		NVIC_SetPriority(USART1_IRQn, UART_INT_PRIO);
	}
	uart_dma_enable();

}

#if UART_DMA_INT_MODE == 1
void USART1_DMA_Channel5_InterruptHandler(void) {

	//flaga przerwania ukonczenia transferu dla kanulu 5 ustawiona
	if ((DMA1->ISR & DMA_ISR_TCIF5) == DMA_ISR_TCIF5) {

		//LED_TOGGLE();


		//wyczyszczenie flagi przerwania
		DMA1->IFCR |= (DMA_IFCR_CTCIF5);
	}

}
#endif



void UART1_Idle_Rx_interrupt_handler(void) {


	if((USART1->SR & USART_CR1_IDLEIE) != 0) {
		//LED_TOGGLE();

		uart_read_ptr = uart_write_ptr;
		uart_write_ptr = (RECV_BUFF_SIZE - DMA1_Channel5->CNDTR);
		dev_status.uart_buf_wrapped_around = (uart_write_ptr > uart_read_ptr) ? 0 : 1;
		uint8_t len = (uart_write_ptr > uart_read_ptr) ? (uart_write_ptr - uart_read_ptr) : ((RECV_BUFF_SIZE - uart_read_ptr) + uart_write_ptr);
		//printf("w:%d r:%d len:%d\n",uart_write_ptr,uart_read_ptr,len);

		if(len == REQUEST_FRAME_SIZE) {

			if(dev_status.uart_buf_wrapped_around) {
				memcpy((uint8_t*)&request,&recv_buf[uart_read_ptr],(RECV_BUFF_SIZE - uart_read_ptr));
				memcpy(((uint8_t*)&request)[RECV_BUFF_SIZE - uart_read_ptr],&recv_buf[0],uart_write_ptr);
			}
			else {
				memcpy((uint8_t*)&request,&recv_buf[uart_read_ptr],len);
			}
			dev_status.uart_buf_wrapped_around = 0;
			dev_status.request_received = 1;
		}
		else {
			uart_read_ptr = uart_write_ptr = 0;
			memset(recv_buf,0,sizeof(recv_buf));

		}
		//koniecznie trzeba odczytac ten rejestr aby flaga przerwania IDLE zostala skasowana.
		//sekwerncja kasowania polega na odczytaniu rejestry SR a nastepnie DR
		USART1->DR;


	}

}



bool is_msg_recived(void) {
	return dev_status.request_received == 1;
}



void USART1_DMA_Channel5_InterruptHandler(void) {

	//flaga przerwania ukonczenia transferu dla kanulu 5 ustawiona
	if ((DMA1->ISR & DMA_ISR_TCIF5) == DMA_ISR_TCIF5) {

		//LED_TOGGLE();

		//wyczyszczenie flagi przerwania
		DMA1->IFCR |= (DMA_IFCR_CTCIF5);
	}

}



void uart_send(uint8_t * data, uint16_t len) {

	for(uint16_t i=0; i<len; ++i) {
		USART1->DR = data[i];
		//czekaj na zakonczenie transmisji
		while((USART1->SR & USART_SR_TC) != USART_SR_TC) {}
		//transmisja zakonczona - skasuj flage TC
		USART1->SR &= ~(USART_SR_TC);
	}
}

void uart_receive(uint8_t *data, uint16_t len) {
	for (uint16_t i = 0; i < len; ++i) {
		//czekaj az w rejestrze odbiorczym pojawi sie jakas dana
		while ((USART1->SR & USART_SR_RXNE) == 0) {
		}
		data[i] = USART1->DR;
		USART1->SR &= ~(USART_SR_RXNE);
	}
}


void send_response_msg(void) {

	uart_send((uint8_t*)&pwm_mframe,sizeof(measure_frame_t));
}




void send_char(char c)
{
	uart_send(&c, 1);
}



int get_char(void) {
	uint8_t value = -1;
	uart_receive(&value, 1);
	return value;
}



int __io_putchar(int ch)
{
	if (ch == '\n')
		send_char('\r');
	send_char(ch);
	return ch;
}


int __io_getchar(void) {

	int c = -1;
	c = get_char();
	send_char(c); //echo
	if(c == '\n' || c == '\r') {
		send_char('\n');
		send_char('\r');
		return '\n';
	}
	return c;
}




void ClocksStatus(void) {

	char msg[128] = {[0]=0};

	memset(msg,0,sizeof(msg));
	sprintf(msg,"SYSCLOCK: %ld\r\n",HAL_RCC_GetSysClockFreq());
	printf(msg);

	memset(msg,0,sizeof(msg));
	sprintf(msg,"HCLK: %ld\r\n",HAL_RCC_GetHCLKFreq());
	printf(msg);

	memset(msg,0,sizeof(msg));
	sprintf(msg,"PCLK1: %ld\r\n",HAL_RCC_GetPCLK1Freq());
	printf(msg);

	memset(msg,0,sizeof(msg));
	sprintf(msg,"PCLK2: %ld\r\n",HAL_RCC_GetPCLK2Freq());
	printf(msg);

	printf("*******************************************\r\n");
}



void adc1_disable(void) {
	if ((ADC1->CR2 & ADC_CR2_ADON) == ADC_CR2_ADON) {
		ADC1->CR2 &= ~(ADC_CR2_ADON);
		//czekaj na wylaczenie ADC
		//while ((ADC1->CR2 & ADC_CR2_ADON) == ADC_CR2_ADON) { char x = 'a'; uart_send(&x,1); }
		while ((ADC1->CR2 & ADC_CR2_ADON) == ADC_CR2_ADON) { }
	}
}

void adc1_enable(void) {
	if ((ADC1->CR2 & ADC_CR2_ADON) == 0) {
		ADC1->CR2 |= (ADC_CR2_ADON);
	}
}


#define DMA_INTERRUPT_MODE 1


void adc_dma_scan_init(void) {





	if ((RCC->APB2ENR & RCC_APB2ENR_IOPAEN) == 0) {
		//wlaczenie zegara rejestru GPIOA
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
		//wlaczenie zegara funkcji alternatywnych dla pinow portu
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	}


	if ((RCC->AHBENR & RCC_AHBENR_DMA1EN) == 0) {
		//wlaczenie zegara DMA
		RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	}

	if ((RCC->APB2ENR & RCC_APB2ENR_ADC1EN) == 0) {
		//wlaczenie zegara ADC1
		RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	}

	//zatrzymaj adc z oczekiwaniem na zakonczenie biezacej konwersji
	adc1_disable();

	//Reset konfiguracji ADC do stanu z wlaczenia procka - uniewaznienie starej konfiguracji aby zainicjalizowac ponownie

	RCC->APB2RSTR |= RCC_APB2RSTR_ADC1RST;
	HAL_Delay(1);
	RCC->APB2RSTR &= ~(RCC_APB2RSTR_ADC1RST);



	//konfiguracja pinu jako wejsciowy
	GPIOA->CRL &= ~( GPIO_CRL_MODE4_0 | GPIO_CRL_MODE4_1);
	//konfiguracja pinu PA4 jako analgowy
	GPIOA->CRL &= ~(GPIO_CRL_CNF4_0 | GPIO_CRL_CNF4_1);


	//Przy ponownej re-inicjalizacji DMA koniecznie trzeba wylaczyc DMA aby zmiany zostaly zapisane
	DMA1_Channel1->CCR &= ~DMA_CCR_EN;

	//Przypisanie adresu zrodlowego peryferium skad pobierane beda dane i transferowane do pamieci
	DMA1_Channel1->CPAR = (uint32_t) (&(ADC1->DR));
	//przypisanie adresu pamieci do ktorego zapisywane beda dane
	DMA1_Channel1->CMAR = (uint32_t) (pwm_amp_probe);
	//ilosc transakcji DMA (tozsame z rozmiarem bufora). Gdy wszystkie transakcje zostana wywolane
	//to ten rejestr ma wartosc 0. Zadana nowa transkacja nie moze byc obsluzona jezeli ponownie sie
	//go nie zaprogramuje na niezerowa wartosc. Aby to zrobic wczesniej trzeba wylaczyc DMA, poniewaz
	//gdy DMA jest wlaczone ten rejestr jest tylko read-only

	DMA1_Channel1->CNDTR = PWM_AMP_PROBE_LEN;


	//ustawienie priorytetu kanalu DMA - priorytet najnizszy
	DMA1_Channel1->CCR &= ~(DMA_CCR_PL_0 | DMA_CCR_PL_1);
	//rozmiar danych danych w pamieci
	DMA1_Channel1->CCR |= DMA_CCR_MSIZE_0;
	//rozmiar danych w rejestrze peryferium
	DMA1_Channel1->CCR |= DMA_CCR_PSIZE_0;

	//auto inkrementacja adresu pamieci do ktorego ladowane beda dane
	//wlaczenie przerwan od ukonczenia transferu - przerwanie generowane jest po wszystkich transakcjach okreslonych przez CNDTR

#if DMA_INTERRUPT_MODE == 1
	DMA1_Channel1->CCR |= (DMA_CCR_MINC | DMA_CCR_TCIE | DMA_CCR_CIRC);

	if (__NVIC_GetEnableIRQ(DMA1_Channel1_IRQn) == 0) {
		NVIC_EnableIRQ(DMA1_Channel1_IRQn);
		NVIC_SetPriority(DMA1_Channel1_IRQn, ADC_INT_PRIO);
	}
#else

	DMA1_Channel1->CCR |= (DMA_CCR_MINC | DMA_CCR_CIRC);
	//DMA1_Channel1->CCR |= (DMA_CCR_MINC);

#endif

	//wlaczenie DMA
	DMA1_Channel1->CCR |= DMA_CCR_EN;




	//ustawienie liczby konwersji na 8 (liczy sie od 0 wiec programujemy 7) - teraz dla kazdego slowa SQx w rejstrze SQR3 nalezy ustawic
	//kanal 4. wtedy pierwszy pomiar w sekwencji to SQ1 na kanale 4, drugi to SQ2 na kanale 4 itd.
	ADC1->SQR1 |= (ADC_SQR1_L_2 |  ADC_SQR1_L_1 |  ADC_SQR1_L_0);

	//wybor kanalu 4 jako 1-szy w sekwencji
	ADC1->SQR3 |= ADC_SQR3_SQ1_2;
	//wybor kanalu 4 jako 2 w sekwencji
	ADC1->SQR3 |= ADC_SQR3_SQ2_2;
	//wybor kanalu 4 jako 3 w sekwencji
	ADC1->SQR3 |= ADC_SQR3_SQ3_2;
	//wybor kanalu 4 jako 4 w sekwencji
	ADC1->SQR3 |= ADC_SQR3_SQ4_2;
	//wybor kanalu 4 jako 5 w sekwencji
	ADC1->SQR3 |= ADC_SQR3_SQ5_2;
	//wybor kanalu 4 jako 6 w sekwencji
	ADC1->SQR3 |= ADC_SQR3_SQ6_2;
	//wybor kanalu 4 jako 7 w sekwencji
	ADC1->SQR2 |= ADC_SQR2_SQ7_2;
	//wybor kanalu 4 jako 8 w sekwencji
	ADC1->SQR2 |= ADC_SQR2_SQ8_2;



	//czas samplowania 1.5 cykla
	ADC1->SMPR2 &= ~(ADC_SMPR2_SMP4_2 | ADC_SMPR2_SMP4_1 | ADC_SMPR2_SMP4_0);

	//wyzwalanie pomiaru adc z zewnetrznego zrodla
	ADC1->CR2 |= ADC_CR2_EXTTRIG;
	//wybranie zdarzenia jakie trigeruje pomiar - ustawienie bitu SWSTART
	//ADC1->CR2 |= (ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_1 |ADC_CR2_EXTSEL_2);
	//pomiar wyzwalany timerem
	ADC1->CR2 |= ADC_CR2_EXTSEL_2;
	ADC1->CR2 &= ~(ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_1);
	//wlaczenie DMA w ADC
	ADC1->CR2 |= ADC_CR2_DMA;

	ADC1->CR1 |= ADC_CR1_SCAN;

	//wlaczenie przetwornika
	ADC1->CR2 |= ADC_CR2_ADON;


}



uint16_t calculate_signal_amp(void) {
	return (calcAvgPulseHigh(pwm_measurements_probes,PWM_MEASUREMENTS_PROBES * PWM_AMP_PROBE_LEN) * ADC_SCALE_FACTOR * VOLTAGE_DIVIDER_SCALE_FACTOR) * 100;
}


#if DMA_INTERRUPT_MODE == 0
void adc_start_pulse_measure(void) {

	memset(pwm_amp_probe, 0, sizeof(pwm_amp_probe));
	//pomiar odpalany jest programowo za kazdym razem gdy ustawi sie ponizszy bit
	ADC1->CR2 |= ADC_CR2_SWSTART;
	//poczekaj na zakonczenie transferu DMA - flaga TCIF1 zapala sie gdt wsztstkie transakcje okreslone w CNDTR zostana dokonane
	while ((DMA1->ISR & DMA_ISR_TCIF1) == 0) {}
	DMA1->IFCR |= DMA_IFCR_CTCIF1;

}
#else
void adc_start_pulse_measure_it(void) {

	if((DMA1->ISR & DMA_ISR_TCIF1) == 0) {
		memset(pwm_amp_probe, 0, sizeof(pwm_amp_probe));
		//pomiar odpalany jest programowo za kazdym razem gdy ustawi sie ponizszy bit
		ADC1->CR2 |= ADC_CR2_SWSTART;

	}

}
#endif


void print_pulse_probe(void) {
	for(uint8_t i=0; i<PWM_AMP_PROBE_LEN;++i) {
		printf("%d ",pwm_amp_probe[i]);
	}
	printf("\n");
}

void print_probes(void) {
	for (uint8_t i = 0; i < PWM_MEASUREMENTS_PROBES; ++i) {
		printf("%d | ", pwm_measurements_probes[i]);
	}
	printf("\n");
}

void ADC1_DMA_Channel1_InterruptHandler(void) {


	//flaga przerwania ukonczenia transferu dla kanulu 1 ustawiona
	if( (DMA1->ISR & DMA_ISR_TCIF1) == DMA_ISR_TCIF1) {


		memcpy(&pwm_measurements_probes[pwm_new_probe_ptr % (PWM_MEASUREMENTS_PROBES * PWM_AMP_PROBE_LEN)],pwm_amp_probe,PWM_AMP_PROBE_LEN);
		pwm_new_probe_ptr+= 8;
		if(pwm_new_probe_ptr == (PWM_MEASUREMENTS_PROBES * PWM_AMP_PROBE_LEN))  {
			//po zebraniu 4 probek zatrzymaj dalszey pomiar chyba ze pwm zanikl i odbywa sie pomiar sygnalu stalego
			//wtedy nie stopuj timera trigerujacego pomiary
			if(dev_status.const_signal_measure_mode == 0) {
				tim3_adc_trigger_stop();
			}
			pwm_new_probe_ptr = 0;
		}

		DMA1->IFCR |= (DMA_IFCR_CTCIF1);


	}


}







#define TIM3_INTERRUPT_MODE 0


void tim3_adc_trigger_stop(void) {
	if ((TIM3->CR1 & TIM_CR1_CEN) == TIM_CR1_CEN) {
#if TIM3_INTERRUPT_MODE == 1
		TIM3->DIER &= ~(TIM_DIER_UIE);
		TIM3->SR &= ~(TIM_SR_UIF);
#endif
		TIM3->CR1 &= ~(TIM_CR1_CEN);
		TIM3->CNT = 0;
	}
}

void tim3_adc_trigger_start(void) {
	if ((TIM3->CR1 & TIM_CR1_CEN) == 0) {
		TIM3->CNT = 0;
#if TIM3_INTERRUPT_MODE == 1
		TIM3->SR &= ~(TIM_SR_UIF);
		TIM3->DIER |= (TIM_DIER_UIE);
#endif
		TIM3->CR1 |= TIM_CR1_CEN;
	}
}


void tim3_adc_trigger_init(void) {

	//uruchomienie zegara tim 3
	if((RCC->APB1ENR & RCC_APB1ENR_TIM3EN) == 0) {
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	}


	tim3_adc_trigger_stop();

	RCC->APB1RSTR |= RCC_APB1RSTR_TIM3RST;
	HAL_Delay(1);
	RCC->APB1RSTR &= ~(RCC_APB1RSTR_TIM3RST);

	//ustawienie ze tylko counter overflow generuje zdarzenie trigerujace update interrupt albo dma request
	TIM3->CR1 |= TIM_CR1_URS;

	//timer w trybie master gerenrujacy sygnal TRGO dla podpietego peryterium przy pprzelenieniu licznika
	TIM3->CR2 |= TIM_CR2_MMS_1;
	TIM3->PSC = p_config.pwm_adc_take_probe_prescaler_tim3 - 1; //zegar APB1 = 64 MHz. 64MHz / 6400 = 1 MHz
	TIM3->ARR = p_config.pwm_adc_take_probe_period_in_cycles_tim3 - 1; //liczba cykli po ktorej timer sie przepelnia
	//wlaczenie przerwan overflow
#if TIM3_INTERRUPT_MODE == 1
	TIM3->DIER |= (TIM_DIER_UIE);

	if (__NVIC_GetEnableIRQ(TIM3_IRQn) == 0) {
		NVIC_SetPriority(TIM3_IRQn, TIM3_DELAY_BEFORE_MEASURE_PRIO);
		NVIC_EnableIRQ(TIM3_IRQn);
	}
#endif
	TIM3->CNT = 0;


}





void tim3_overflow_interrupt_handler(void) {



}



void tim2_watchodog_timer_start(void) {

	if ((TIM2->CR1 & TIM_CR1_CEN) == 0) {
		TIM2->CNT = 0;
		TIM2->DIER |= TIM_DIER_UIE;
		TIM2->CR1 |= TIM_CR1_CEN;
	}
}


void tim2_watchodog_timer_stop(void) {

	if ((TIM2->CR1 & TIM_CR1_CEN) == TIM_CR1_CEN) {
		//zablokowanie przerwan
		TIM2->DIER &= ~(TIM_DIER_UIE);
		TIM2->CR1 &= ~(TIM_CR1_CEN);
		TIM2->CNT = 0;
	}
}


void tim2_const_signal_watchdog_init(void) {

	dev_status.ignore_watchdog_tim2_interrupt = 1;
	//uruchomienie zegara timera
	if ((RCC->APB1ENR & RCC_APB1ENR_TIM2EN) == 0) {
		RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	}

	tim2_watchodog_timer_stop();

	RCC->APB1RSTR |= RCC_APB1RSTR_TIM2RST;
	HAL_Delay(1);
	RCC->APB1RSTR &= ~(RCC_APB1RSTR_TIM2RST);


	//przerwanie moze wygenerowac tylko update interrupt
	TIM2->CR1 |= TIM_CR1_URS;

	TIM2->PSC = p_config.const_signal_watchdog_prescaler_tim2 - 1;
	TIM2->ARR = p_config.const_signal_watchdog_period_in_cycles_tim2 - 1;
	//wlaczenie przerwan update interrupt
	TIM2->DIER |= TIM_DIER_UIE;
	TIM2->CNT = 0;

	if(__NVIC_GetEnableIRQ(TIM2_IRQn) == 0) {
		NVIC_EnableIRQ(TIM2_IRQn);
    	NVIC_SetPriority(TIM2_IRQn,TIM2_WATCHDOG_CONST_SIG_PRIO);
	}


}




void TIM2_const_signal_watchdog_interrupt_handler(void) {

	if((TIM2->SR & TIM_SR_UIF) == TIM_SR_UIF) {
		dev_status.const_signal_measure_mode = 1;
		TIM2->SR &= ~(TIM_SR_UIF);
	}
}


void const_signal_measure(void) {

	if(dev_status.const_signal_measure_started == 0) {
		tim3_adc_trigger_start();
		dev_status.const_signal_measure_started = 1;
	}

	InputPWMDuty_percent = (calculate_signal_amp() <= LOW_LEVEL_TRESHOLD ? 0 : 100);
	InputPWMFrequency = 0;
}







void pwm_edge_detect_start(void) {

	if ((TIM1->CR1 & TIM_CR1_CEN) == 0) {
		TIM1->CNT = 0;
		TIM1->CCER |= (TIM_CCER_CC1E | TIM_CCER_CC2E);
		//nalezy wlaczyc timer poniewaz w przeciwnym razie detekcja zboczy i przerwanie bedzie sie odbywac
		//ale wartoscia zapisywana do rejestrow capture bdzie 0 bo timer nie liczy
		TIM1->CR1 |= TIM_CR1_CEN;
	}
}


void pwm_edge_detect_stop(void) {

	if ((TIM1->CR1 & TIM_CR1_CEN) == TIM_CR1_CEN) {
		TIM1->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC2E);
		TIM1->CR1 &= ~TIM_CR1_CEN;
		TIM1->CNT = 0;
	}

}


void pwm_edge_detection_init(void) {


	    __HAL_RCC_GPIOA_CLK_ENABLE();
	    //TIM1 GPIO Configuration
	    //PA8     ------> TIM1_CH1

	    GPIO_InitTypeDef GPIO_InitStruct = {0};
	    GPIO_InitStruct.Pin = GPIO_PIN_8;
	    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);





	if ((RCC->APB2ENR & RCC_APB2ENR_TIM1EN) == 0) {
		//uruchomienie zegara timera 32 MHz
		RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	}

	pwm_edge_detect_stop();

	RCC->APB2RSTR |= RCC_APB2RSTR_TIM1RST;
	HAL_Delay(1);
	RCC->APB2RSTR &= ~(RCC_APB2RSTR_TIM1RST);

	TIM1->PSC = p_config.pwm_meas_period_prescaler_tim1 - 1;
	TIM1->ARR = p_config.pwm_meas_period_in_cycles_tim1 - 1;

	//wejscie trigerujace timer to TI1FP1 - jest to sygnal wykryty przez Edge Detector na Channel 1 i odpowiednie zsamplowany
	TIM1->SMCR |= TIM_SMCR_TS_0 | TIM_SMCR_TS_2;
	//konfiguracja w trybie Slave Reset - wraz ze zboczem narastajacym licznik jest zerowany
	TIM1->SMCR |= TIM_SMCR_SMS_2;

	//przechwycona wartosc licznika bedzie skladowana w TIM1_CCR1
	//Channel 1 skonfigurowany jako input oraz IC1 zmapowany za wejsci TI1
	TIM1->CCMR1 |= TIM_CCMR1_CC1S_0;
	//trigeruje sygnal capture IC1 na zboczu narastajacym
	TIM1->CCER &= ~(TIM_CCER_CC1P);

	//Channel 1 skonfigurowany jako input oraz IC2 zmapowany za wejsci TI1
	TIM1->CCMR1 |= TIM_CCMR1_CC2S_1;
	//trigeruje sygnal capture IC2 na zboczu opadajacym
	TIM1->CCER |= (TIM_CCER_CC2P);

	//wlaczenie przechwytywania zboczy do rejestru TIM_CCR1 i TIM_CCR2 na kanale 1
	TIM1->CCER |= (TIM_CCER_CC1E | TIM_CCER_CC2E);

	//wlaczenie przerwan od zboczy narastajacych i opadajacych
	TIM1->DIER |= (TIM_DIER_CC1IE | TIM_DIER_CC2IE);

	//nalezy wlaczyc timer poniewaz w przeciwnym razie detekcja zboczy i przerwanie bedzie sie odbywac
	//ale wartoscia zapisywana do rejestrow capture bdzie 0 bo timer nie liczy
	TIM1->CR1 |= TIM_CR1_CEN;


    NVIC_EnableIRQ(TIM1_CC_IRQn);
    NVIC_SetPriority(TIM1_CC_IRQn,PWM_EDGE_DETECT_PRIO);


}





void InputCapture1_interrupt_handler(void) {


	//flaga przerwania od zbocza narastajacego - kasowana przez odczyt rejestru TIM_CCR1 lub recznie
	if((TIM1->SR & TIM_SR_CC1IF) == TIM_SR_CC1IF) {

		dev_status.const_signal_measure_mode = 0;
		dev_status.const_signal_measure_started = 0;

		tim3_adc_trigger_stop();
		tim2_watchodog_timer_stop();

		InputPWMPeriod_cycles = TIM1->CCR1 + 1;

		if(InputPWMPeriod_cycles != 0) {
			InputPWMDuty_percent = 100 - ((InputPWMDuty_cycles) * 100 / (InputPWMPeriod_cycles)); //na plytce jest inwerter
			InputPWMPeriod_miliseconds = ((InputPWMPeriod_cycles * InputTimerPeriodDuration_miliseconds) / (InputTimerResolution_cycles));
			InputPWMFrequency = ((1.0 * 1000) / InputPWMPeriod_miliseconds); //1000 bo skalowanie jednostem ms
		}

		TIM1->SR &= ~(TIM_SR_CC1IF);

	}
	//flaga przerwania od zbocza opadajacego- kasowana przez odczyt rejestru TIM_CCR2 lub recznie
	else if((TIM1->SR & TIM_SR_CC2IF) == TIM_SR_CC2IF) {

		dev_status.const_signal_measure_mode = 0;
		dev_status.const_signal_measure_started = 0;

		InputPWMDuty_cycles = TIM1->CCR2 + 1;

		tim2_watchodog_timer_start();
		tim3_adc_trigger_start();

		TIM1->SR &= ~(TIM_SR_CC2IF);

	}

}






void prepare_data_package(void) {

	memset((uint8_t*)&pwm_mframe,0,sizeof(measure_frame_t));
	pwm_mframe.pwmDuty = InputPWMDuty_percent;
	pwm_mframe.pwmFrq = InputPWMFrequency;
	pwm_mframe.pwmVoltage_HIGH = calculate_signal_amp();

	memset((uint8_t*)pwm_measurements_probes,0,sizeof(pwm_measurements_probes));

	pwm_mframe.pwmVoltage_LOW = 0;
	pwm_mframe.resPP = (((pp_temp_mframe.pp_vol * ADC_SCALE_FACTOR) * PP_VOLTAGE_SCALE_FACTOR) / PP_CURRENT) * 10; //dokladnosc do 1 miejsca po przecinku
	pwm_mframe.tempSensor1 = (pp_temp_mframe.temp1_vol * ADC_SCALE_FACTOR) * 100; //dokladnosc do 2 miejsc po przecinku
	pwm_mframe.tempSensor2 = (pp_temp_mframe.temp2_vol * ADC_SCALE_FACTOR) * 100; //dokladnosc do 2 miejsc po przecinku

	pwm_mframe.CRC16 = calculateCRC16((uint8_t*)&pwm_mframe, sizeof(measure_frame_t)-2); //gdy oblicza sie CRC nie uzywa sie do obliczen pola CRC

}




void print_request(void) {

	for(uint8_t i=0; i<sizeof(request_msg_t); ++i) {
		printf("%d ",((uint8_t*)&request)[i]);
	}
	printf("\n");
}





EV_STATE_t ev_state = EV_STATE_B;


void ev_state_machine(void)
{

	request_msg_t *  r = get_request_msg();

	if(ev_state == (EV_STATE_t)(r->order)) return;

	ev_state = (EV_STATE_t)(r->order);
	//jezeli przyjdzie nie zwalidowana ramka to rzutowanie na enum jest undefined,
	//wtedy moze sie zalaczyc domyslny stan B lub jakis inny stan w zaleznosci od wyniku
	//rzutowania liczby na typ enum.

	switch(ev_state) {


	case EV_STATE_C: {

		Res_2K74_set(GPIO_PIN_SET);
		Res_1K3_set(GPIO_PIN_SET);
		Res_270R_set(GPIO_PIN_RESET);
		ev_state = EV_STATE_C;

		break;
	}
	case EV_STATE_D: {

		Res_2K74_set(GPIO_PIN_SET);
		Res_1K3_set(GPIO_PIN_RESET);
		Res_270R_set(GPIO_PIN_SET);
		ev_state = EV_STATE_D;

		break;
	}

	case NO_CHANGE: {

		break;
	}
	default:
	case EV_STATE_B: {
			Res_2K74_set(GPIO_PIN_SET);
			Res_1K3_set(GPIO_PIN_RESET);
			Res_270R_set(GPIO_PIN_RESET);
			ev_state  = EV_STATE_B;

		}

	}

}


EV_STATE_t get_ev_state(void) {
	return ev_state;
}










void adc2_stop(void) {
	if ((ADC2->CR2 & ADC_CR2_ADON) == ADC_CR2_ADON) {
		ADC2->CR2 &= ~(ADC_CR2_ADON);
		//czekaj na wylaczenie ADC
		while ((ADC2->CR2 & ADC_CR2_ADON) == ADC_CR2_ADON);
	}
}

void adc2_start(void) {
	if ((ADC2->CR2 & ADC_CR2_ADON) == 0) {
		ADC2->CR2 |= (ADC_CR2_ADON);
		while ((ADC2->CR2 & ADC_CR2_ADON) == 0);
	}
}

void adc2_single_conv_reconfigure_channel(enum adc2_channel_choose_t ch) {

	//jezeli ADC jest wlaczone, to wylacz

	static enum adc2_channel_choose_t cc = NO_CAHNNEL;
	if(cc == ch) return;

	adc2_stop();

	ADC2->SQR3 &= ~(ADC_SQR3_SQ1_0 | ADC_SQR3_SQ1_1 | ADC_SQR3_SQ1_2 | ADC_SQR3_SQ1_3 | ADC_SQR3_SQ1_4);

	if(ch == TEMP1_CHANNEL) {
		//pierwsza konwersja SQ1 - kanal 1 (PA1)
		ADC2->SQR3 |= ADC_SQR3_SQ1_0;
		//samplowanie dla kanalu PA1: 239 cykli zegara
		ADC2->SMPR2 |= ADC_SMPR2_SMP1_0 | ADC_SMPR2_SMP1_1 | ADC_SMPR2_SMP1_2;
	}
	else if(ch == TEMP2_CHANNEL) {
		//druga konwersja SQ2 - kanal 2 (PA2)
		ADC2->SQR3 |= ADC_SQR3_SQ1_1;
		ADC2->SMPR2 |= ADC_SMPR2_SMP2_0 | ADC_SMPR2_SMP2_1 | ADC_SMPR2_SMP2_2;
	}
	else if(ch == PP_CHANNEL) {
		//trzecia konwersja SQ3 - kanal 2 (PA3)
		ADC2->SQR3 |= ADC_SQR3_SQ1_0 | ADC_SQR3_SQ1_1;
		ADC2->SMPR2 |= ADC_SMPR2_SMP3_0 | ADC_SMPR2_SMP3_1 | ADC_SMPR2_SMP3_2;
	}
	cc = ch;

}



void adc2_pp_temp_measure_init(void) {


	 //wlaczenie zegara funkcji alternatywnych dla pinow
	if ((RCC->APB2ENR & RCC_APB2ENR_IOPAEN) == 0) {

		//wlazcenie zegara dla portu A
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	}




	 //ustawienie pinu PA1 w trybie analog oraz w trybie input
	 GPIOA->CRL &= ~(GPIO_CRL_CNF1_0 |  GPIO_CRL_CNF1_1);
	 GPIOA->CRL &= ~(GPIO_CRL_MODE1_0 | GPIO_CRL_MODE1_1);

	//ustawienie pinu PA2  w trybie analog oraz w trybie input
	 GPIOA->CRL &= ~(GPIO_CRL_CNF2_0 |  GPIO_CRL_CNF2_1);
     GPIOA->CRL &= ~(GPIO_CRL_MODE2_0 | GPIO_CRL_MODE2_1);

     //ustawienie pinu PA3  w trybie analog oraz w trybie input
     GPIOA->CRL &= ~(GPIO_CRL_CNF3_0 |  GPIO_CRL_CNF3_1);
     GPIOA->CRL &= ~(GPIO_CRL_MODE3_0 | GPIO_CRL_MODE3_1);



	 if((RCC->APB2ENR & RCC_APB2ENR_ADC2EN) == 0 ) {

		 RCC->APB2ENR |= RCC_APB2ENR_ADC2EN;

	 }

	adc2_stop();

	RCC->APB2RSTR |= RCC_APB2RSTR_ADC2RST;
	HAL_Delay(1);
	RCC->APB2RSTR &= ~(RCC_APB2RSTR_ADC2RST);

	//trigerowanie pomiaru zewnetrznm zdarzeniem
	ADC2->CR2 |= ADC_CR2_EXTTRIG;
	//zdarzenie trigerujace: ustawiebie bitu SWSTART
	ADC2->CR2  |= (ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_2);

	adc2_single_conv_reconfigure_channel(TEMP1_CHANNEL);

	//uruchomienie ADC
	adc2_start();


}






void adc2_single_conv_start(void) {

	//pomiar odpalany jest programowo za kazdym razem gdy ustawi sie ponizszy bit
	ADC2->CR2 |= ADC_CR2_SWSTART;
	while((ADC2->SR & ADC_SR_EOC) == 0);
	ADC2->CR2 &= ~(ADC_SR_EOC);
}


uint16_t adc2_single_conv_measure(enum adc2_channel_choose_t ch) {

	adc2_single_conv_reconfigure_channel(ch);
	adc2_start();
	adc2_single_conv_start();
	return ADC2->DR;
}


void measure_pp_temp(void) {

	memset((uint8_t*)&pp_temp_mframe,0,sizeof(measure_pp_temp_t));

	pp_temp_mframe.temp1_vol = adc2_single_conv_measure(TEMP1_CHANNEL);
	pp_temp_mframe.temp2_vol = adc2_single_conv_measure(TEMP2_CHANNEL);
	pp_temp_mframe.pp_vol = adc2_single_conv_measure(PP_CHANNEL);
}









/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
	peripheral_config();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */






  uart_dma_init();



  adc_dma_scan_init();
  adc2_pp_temp_measure_init();
  tim3_adc_trigger_init();
  tim2_const_signal_watchdog_init();
  pwm_edge_detection_init();

  ev_state_machine();



  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

		if (dev_status.const_signal_measure_mode == 1) {
			const_signal_measure();
		}

		if (is_msg_recived()) {

			if (validate_request()) {

				pwm_edge_detect_stop();

				measure_pp_temp();

				prepare_data_package();
				send_response_msg();

				ev_state_machine();

				dev_status.request_received = 0;

				pwm_edge_detect_start();

			}

		}
		//adc_start_pulse_measure_it();

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB12 PB13 PB14 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
